class Post < ActiveRecord::Base
  attr_accessible :content, :title, :user_id

  validates :title, :presence => true,
                    :length => { :minimum => 5 }

  belongs_to :user
  has_many :comments, :dependent => :destroy
end
