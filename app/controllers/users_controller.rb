class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end
 
  # GET /users/new
  def new
    @user = User.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def login
    if request.post?
      session[:user_id] = nil
      ret = User.authenticate(params[:userid], params[:password])
      if ret
        flash[:message] = "Login Successful"
        session[:user_id] = ret.id
        redirect_to_stored
      else
        flash[:warning] = "Login failed"
      end
    end 
  end

  def logout
    session[:user_id] = nil
    redirect_to :root
  end

  def posts
    u = User.find_by_id(params[:id])
    @username = User.get_user_name(u.id)
    @post_array = u.posts.order('updated_at').reverse_order
  end

end
