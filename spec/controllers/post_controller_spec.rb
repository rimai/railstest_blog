require 'spec_helper'

describe PostsController do
  fixtures :users, :posts

  describe "When logined" do
    before do 
      session[:user_id] = users(:ichiro).id
    end
    describe "GET 'index'" do
      it "returns http success" do
        get 'index'
        response.should be_success
      end
    end

    describe "GET 'show'" do
      it "returns http success" do
        get 'show', {:id => posts(:one).id}
        response.should be_success
      end
    end

    describe "GET 'new'" do
      it "returns http success" do
        get 'new'
        response.should be_success
      end
    end

    describe "GET 'edit'" do
      it "returns http success" do
        get 'edit', {:id => posts(:one).id}
        response.should be_success
      end
    end

    describe "GET 'create'" do
      it "returns http success" do
        get 'create'
        response.should be_success
      end
    end

  end

  describe "When not logined" do
    before do 
      session[:user_id] = nil
    end
    describe "GET 'index'" do
      it "returns http success" do
        get 'index'
        response.should be_success
      end
    end

    describe "GET 'show'" do
      it "returns http success" do
        get 'show', {:id => posts(:one).id}
        response.should be_success
      end
    end

    describe "GET 'new'" do
      it "returns http success" do
        get 'new'
        response.should_not be_success
      end
    end

    describe "GET 'edit'" do
      it "returns http success" do
        get 'edit', {:id => posts(:one).id}
        response.should_not be_success
      end
    end

    describe "GET 'create'" do
      it "returns http success" do
        get 'create'
        response.should_not be_success
      end
    end

  end


end
