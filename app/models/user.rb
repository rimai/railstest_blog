# encoding: utf-8
class User < ActiveRecord::Base
  attr_accessible :password, :username
  
  validates :username, :presence => true, :length => { :minimum => 4, :message => "は４文字以上の必要があります"},
    :uniqueness => true ,
    :format => { :with => /^[a-zA-Z0-9]+$/, :message => "は半角英数のみです" }

  validates :password,
    :format => { :with => /^[!-~]+$/, :message => "は半角英数記号のみです"}

  has_many :posts, :dependent => :destroy

  def self.authenticate(username, password)
    u = get_user(username)
    if !u.nil?
      if u.password == password
        return u
      end
    end
    return nil
  end

  def self.get_user_name(id)
    if !id.nil?
      u = find_by_id(id)
      if !u.nil?
        return u.username 
      end
    end
    return "?unknown User?"
  end

  def self.get_user(id)
    if !id.nil?
      u = find_by_id(id)
      return u
    end
  end
end
