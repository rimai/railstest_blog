class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.column :referrences, :post
      t.column :integer, :user_id
      t.column :text, :content

      t.timestamps
    end
  end
end
