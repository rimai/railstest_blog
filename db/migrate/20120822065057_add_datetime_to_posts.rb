class AddDatetimeToPosts < ActiveRecord::Migration
  def self.up
    add_column(:posts, :updatedt, :datetime, :default => '')
  end

  def self.down
    remove_column(:posts, :updatedt)
  end
end
