# encoding: utf-8
class CommentsController < ApplicationController
  before_filter :login_required

  def index
    respond_to posts_path
  end

  def create

    if request.post?
      @post = Post.find(params[:post_id])
      @comment = @post.comments.build
      @comment.user_id = session[:user_id]
      @comment.content = params[:content]
      if @comment.save
      end
      redirect_to post_path(@post)
    else
      redirect_to post_path(@post)
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    if id_check(@comment.user_id)
      post_id = @comment.post_id
      @comment.destroy

      respond_to do |format|
        format.html { redirect_to post_path(post_id), :notice => "コメントを削除しました" }
        format.json { head :no_content }
      end
    else
      redirect_to @post, :alert => "他人のコメントは削除できません"
    end
  end

end
