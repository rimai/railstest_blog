# encoding: utf-8
require 'spec_helper'

describe UsersController do
  fixtures :users, :posts

  describe "indexをGETすると" do
    before do 
      get :index
    end

    it "httpリクエストが成功する" do
      get :index
      response.should be_success
    end 
  end

  describe "indexをGETすると" do
    before do 
      get :index
    end

    it "httpリクエストが成功する" do
      get :index
      response.should be_success
    end 
  end

  describe "newをGETすると" do
    before do 
      get :new
    end

    it "httpリクエストが成功する" do
      response.should be_success
    end
  end

  describe "存在するUserでuser/:id/postsをGETすると" do
    before do
      get :posts, :id => users(:ichiro).id
    end

    it "httpリクエストが成功する" do
      get :posts, :id => 0
      response.should be_success
    end

    it "@usernameに対応するusernameが入る" do
      assigns[:username].should == User.get_user_name(users(:ichiro).id)
    end

    it "@post_arrayにpostsが入る" do
      assigns[:post_array].should == users(:ichiro).posts.order('updated_at').reverse_order
    end
  end

  describe "存在しないUserでuser/:id/postsをGetすると" do
    it "RuntimeErrorが起きる" do
      expect{ get :posts, :id => 100000 }.to raise_error(RuntimeError)
    end
  end

  describe "'create'をGETすると" do
    it "returns http success" do
      get 'create'
      response.should be_success
    end
  end

  describe "'login'をGETすると" do
    before do 
      get 'login'
    end

    it "returns http success" do
      response.should be_success
    end
  end

  describe "'login'に存在するユーザでPOSTすると" do
    before do
      session[:user_id] = nil
      post 'login', {:userid => users(:ichiro).id, :password => users(:ichiro).password}
    end

    it "ログインに成功する" do
      session[:user_id] = users(:ichiro).id
    end
  end

  describe "'login'に存在するユーザで間違ったパスワードでPOSTすると" do
    before do
      session[:user_id] = nil
      post 'login', {:userid => users(:ichiro).id, :password => users(:ichiro).password + "wrong"}
    end

    it "ログインに失敗する" do
      session[:user_id] = nil
    end
  end

  describe "'login'に存在しないユーザでPOSTすると" do
    before do
      session[:user_id] = nil
      post 'login', {:userid => users(:ichiro).id + 50000, :password => users(:ichiro).password + "wrong"}
    end

    it "ログインに失敗する" do
      session[:user_id] = nil
    end
  end

  describe "ログイン時に'logout'をGETすると" do
    before do
      session[:user_id] = users(:ichiro).id
      get 'logout'
    end
    
    it "ログアウトされる" do
      session[:user_id] = nil
    end
  end


end
