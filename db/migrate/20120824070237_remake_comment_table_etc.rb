class RemakeCommentTableEtc < ActiveRecord::Migration
  def change
    drop_table(:comments)

    remove_column(:posts, :userid)
    remove_column(:posts, :name)

    create_table :comments do |t|
      t.integer :user_id
      t.integer :post_id
      t.string :content
      t.timestamp
    end
    
    change_table :comments do |t|
      t.index :post_id
    end

  end
end
