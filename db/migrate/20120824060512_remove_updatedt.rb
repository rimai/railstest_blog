class RemoveUpdatedt < ActiveRecord::Migration
  def up
    remove_column(:posts, :updatedt)
  end

  def down
    add_column(:posts, :updatedt, :datetime)
  end
end
