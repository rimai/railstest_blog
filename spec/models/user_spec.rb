#encoding: utf-8
require 'spec_helper'

describe User do
  fixtures :users

  describe "Validation" do
    before(:each) do
      @user = User.new
      
      #Validな内容
      @user.username = "saburo"
      @user.password = "passpass"
    end

    it "正しく入力された場合、成功する" do
      @user.should be_valid
    end

    it "ユーザーID(username)が4文字未満の場合、失敗する" do
      @user.username = "333"
      @user.should_not be_valid
    end

    it "ユーザーID(username)が空の場合、失敗する" do
      @user.username = ""
      @user.should_not be_valid
    end

    it "ユーザーID(username)に半角英数以外が使われた場合、失敗する" do
      @user.username = "あいうえお?"
      @user.should_not be_valid
    end

    it "パスワードに半角英数以外が使われていた場合、失敗する" do
      @user.password = "あいうえお"
      @user.should_not be_valid
    end

  end

  describe "ユーザー認証時" do
    it "存在するユーザーでは、認証に成功する" do
      u = users(:ichiro)
      ret = User.authenticate(u.username, u.password)
      ret.should == u
    end

    it "存在するユーザーでパスワードが正しくない場合、認証に失敗する" do
      u = users(:ichiro)
      ret = User.authenticate(u.username, u.password + "aaaaa")
      ret.should == nil
    end

    it "存在しないユーザーでは、認証に失敗する" do
      ret = User.authenticate("abcdefg_username","abcdefg_password")
      ret.should == nil
    end
  end
end
