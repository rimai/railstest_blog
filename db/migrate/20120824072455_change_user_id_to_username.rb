class ChangeUserIdToUsername < ActiveRecord::Migration
  def up
    rename_column :users, :userid, :username
  end

  def down
    rename_column :users, :username, :userid
  end
end
