class ApplicationController < ActionController::Base
  protect_from_forgery

  def login_required
    if session[:user_id]
      return true
    end
    flash[:warning] = "Login"
    session[:return_to] = request.fullpath
    redirect_to :login
    return false
  end

  def redirect_to_stored
    if session[:return_to]
      return_to = session[:return_to]
      session[:return_to] = nil
      redirect_to return_to
    else
      redirect_to posts_path 
    end
  end  

  def id_check(id)
    if session[:user_id] == id
      return true
    else
      return false 
    end
  end
end
